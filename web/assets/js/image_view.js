// Generated by CoffeeScript 1.10.0
(function() {
  define(["marionette"], function(Marionette) {
    var ImageView;
    ImageView = Marionette.ItemView.extend({
      template: "#single-image",
      container: ''
    });
    return ImageView;
  });

}).call(this);

//# sourceMappingURL=image_view.js.map
