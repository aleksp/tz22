requirejs.config({
    baseUrl: "assets/js/",
    paths: {
        text: 'lib/text',
        json2: 'lib/json2',
        jquery: '../vendor/jquery/dist/jquery',
        "jquery-ui": '../vendor/jquery-ui/jquery-ui',
        bootstrap: '../vendor/bootstrap/js/bootstrap.js',
        underscore: '../vendor/underscore/underscore',
        backbone: '../vendor/backbone/backbone',
        marionette: "../vendor/marionette/lib/backbone.marionette"
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["jquery", "underscore", "json2", "text"],
            exports: "Backbone"
        },
        marionette: {
            deps: ["backbone"],
            exports: "Marionette"
        },
        "jquery-ui": ["jquery"]
    }
});