// Generated by CoffeeScript 1.10.0
(function() {
  define(['marionette', 'image_model'], function(Marionette, Image) {
    var ImageCollection;
    ImageCollection = Backbone.Collection.extend({
      initialize: function(models, options) {
        this.album_id = parseInt(options.album_id);
        return this.page = parseInt(options.page);
      },
      model: Image,
      url: function() {
        return '/albums/images/' + this.album_id + '/' + this.page;
      }
    });
    return ImageCollection;
  });

}).call(this);

//# sourceMappingURL=image_collection.js.map
