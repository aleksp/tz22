define ["marionette"], (Marionette) ->
  AlbumChildView = Marionette.ItemView.extend
    template: '#album-item'
    tagName: 'tr'

  AlbumChildView