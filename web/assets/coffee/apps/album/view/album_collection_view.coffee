define ['marionette', 'album_child_view'], (Marionette, AlbumChildView) ->
  AlbumCollectionView = Marionette.CompositeView.extend
    childView: AlbumChildView
    template: "#album-item-collection"
    childViewContainer: "tbody"

  AlbumCollectionView