define ['marionette', 'image_view'], (Marionette, ImageView) ->

  ImageCollectionView = Marionette.CollectionView.extend
    childViewContainer: ''
    childView: ImageView
    template: "#single-image"
    onBeforeShow: ->
      this.collection.fetch
        success: ->
          true

  ImageCollectionView