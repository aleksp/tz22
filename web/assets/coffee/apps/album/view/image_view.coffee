define ["marionette"], (Marionette) ->
  ImageView = Marionette.ItemView.extend
    template: "#single-image"
    container: ''

  ImageView