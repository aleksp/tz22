define [
  'marionette'
  'album_model'
  'album_collection'
  'album_collection_view'
  'app_router'
], (Marionette, Album, AlbumCollection, AlbumCollectionView, AppRouter) ->
  app = new Marionette.Application()
  app.addRegions
    appRegion: "#app-region"

  app.on 'start', () ->
    router = new AppRouter
    Backbone.history.start()

    console.log('App started')

  app