define [
  "marionette"
  "app_region"
  "album_collection"
  "album_collection_view"
  "album_model"
  "image_collection"
  "image_collection_view"

], (Marionette, AppRegion, AlbumCollection, AlbumCollectionView, Album, ImageCollection, ImageCollectionView) ->
  appRegion = new AppRegion

  AlbumController = Marionette.Controller.extend
    home: ->
      console.log 'Home'
      albumColection = new AlbumCollection

      albumColection.fetch
        success: (modColl) ->
          collView = new AlbumCollectionView
            collection: modColl

          appRegion.show collView
      error: ->
        console.log "Error"

    album: (id, page) ->
      if typeof(page) == 'undefined' or page==null
        page = 1

      console.log page

      album = new Album {id: id}
      album.fetch
        success: ->
          console.log "Album read"

          imageCollection = new ImageCollection null, {album_id: album.id, page: page}
          imageCollectionView = new ImageCollectionView
            collection: imageCollection


          AlbumLayout = Marionette.LayoutView.extend
            template: "#album-layout"
            model: album
            regions:
              info: '#album-info'
              images: '#images'
            onBeforeShow: ->
              this.showChildView 'images', imageCollectionView

          albumLayout = new AlbumLayout
          appRegion.show albumLayout


  AlbumController