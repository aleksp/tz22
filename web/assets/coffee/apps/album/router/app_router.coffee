define ['marionette', 'album_controller'], (Marionette, AlbumController) ->
  albumController = new AlbumController
  AppRouter = Marionette.AppRouter.extend
    controller: albumController
    appRoutes:
      '': 'home'
      'album/:id': 'album'
      'album/:id/page/:page': 'album'

  AppRouter