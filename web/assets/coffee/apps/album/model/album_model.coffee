define ['backbone'], (Backbone) ->
  Album = Backbone.Model.extend
    urlRoot: '/albums/view'

  Album