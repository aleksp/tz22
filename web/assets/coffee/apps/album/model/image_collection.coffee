define ['marionette', 'image_model'], (Marionette, Image) ->
  ImageCollection = Backbone.Collection.extend
    initialize: (models, options) ->
      this.album_id = parseInt(options.album_id)
      this.page = parseInt(options.page)
    model: Image
    url: -> '/albums/images/' + this.album_id + '/' + this.page

  ImageCollection