define ['marionette', 'album_model'], (Marionette, Album) ->
  AlbumCollection = Backbone.Collection.extend
    model: Album
    url: '/albums'

  AlbumCollection