<?php

namespace AppBundle\Entity;

use AppBundle\Doctrine\Traits\IntIdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Application user
 *
 * @Entity()
 * @Table(name="users")
 * @HasLifecycleCallbacks()
 */
class User implements UserInterface, \Serializable
{
    use IntIdTrait;

    /**
     * Username
     *
     * @var string
     *
     * @Groups({"show"})
     *
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * User password
     *
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * User email
     *
     * @var string
     *
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email;

    /**
     * If user is active
     *
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * User's albums
     *
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="AppBundle\Entity\Album", mappedBy="author")
     */
    private $albums;

    /**
     * Username getter
     *
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * For testing purpose only!
     *
     * @todo Change this method in real application
     *
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Getter for password
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get user roles
     *
     * @return array
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * Erase user credentials
     */
    public function eraseCredentials()
    {
    }

    /**
     * Serialize user entity
     *
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * Unserialize user entity
     *
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param boolean $isActive
     */
    public function setActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @param ArrayCollection $albums
     */
    public function setAlbums($albums)
    {
        $this->albums = $albums;
    }


}