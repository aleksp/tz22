<?php
/*
 * Файл: AlbumImage.php
 * 
 * Создан: 22.02.16
 * 
 * Часть программного продукта: martest_symfony
 *
 * TODO: Дополнительные условия
 */

namespace AppBundle\Entity;


use AppBundle\Doctrine\Traits\IntIdTrait;
use AppBundle\Doctrine\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Image entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="album_images")
 * @ORM\HasLifecycleCallbacks()
 */
class AlbumImage
{
    use IntIdTrait;
    use TimestampableTrait;

    /**
     * Album
     *
     * @var Album
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Album", inversedBy="images")
     * @ORM\JoinColumn(name="album_id", referencedColumnName="id")
     */
    private $album;

    /**
     * Image title
     *
     * @var string
     *
     * @Groups({"show"})
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * Image description
     *
     * @var string
     *
     * @Groups({"show"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * Image path
     *
     * @var string
     *
     * @Groups({"show"})
     *
     * @ORM\Column(type="string")
     */
    private $path;

    /**
     * @return Album
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * @param Album $album
     */
    public function setAlbum($album)
    {
        $this->album = $album;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


}