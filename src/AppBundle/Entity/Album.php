<?php

namespace AppBundle\Entity;

use AppBundle\Doctrine\Traits\IntIdTrait;
use AppBundle\Doctrine\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Album entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="albums")
 * @ORM\HasLifecycleCallbacks
 */
class Album
{
    use IntIdTrait;
    use TimestampableTrait;
    /**
     * Album title
     *
     * @var string
     *
     * @Groups({"show"})
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * Album  description (optional)
     *
     * @var string
     *
     * @Groups({"show"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * Album author
     *
     * @var User
     * @see User::albums
     *
     * @Groups({"show"})
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="albums")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $author;

    /**
     * Album images
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AlbumImage", mappedBy="album")
     */
    private $images;

    /**
     * @var array
     */
    private $pageData;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return int
     *
     * @Groups({"show"})
     */
    public function getImagesCount()
    {
        return $this->images->count();
    }

    /**
     * @Groups({"dont_show"})
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ArrayCollection $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return array
     */
    public function getPageData()
    {
        return $this->pageData;
    }

    /**
     * @Groups({"show"})
     *
     * @param array $pageData
     */
    public function setPageData($pageData)
    {
        $this->pageData = $pageData;
    }


}