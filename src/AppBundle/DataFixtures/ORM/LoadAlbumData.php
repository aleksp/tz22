<?php
/*
 * Файл: LoadAlbumData.php
 * 
 * Создан: 22.02.16
 * 
 * Часть программного продукта: martest_symfony
 *
 * TODO: Дополнительные условия
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Album;
use AppBundle\Entity\AlbumImage;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Data fixture for create albums and images data
 */
class LoadAlbumData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository('AppBundle:User')->findAll();
        foreach ($users as $user) {
            $max = rand(1, 7);
            for ($i = 1; $i < $max; $i++) {
                $album = new Album();
                $album->setTitle(sprintf('Album %s', (string)$i));
                $album->setDescription(rand(0, 9) > 5 ? sprintf('Album %s %s', (string)$i, 'description') : null);
                $album->setAuthor($user);

                $manager->persist($album);
            }
        }
        $manager->flush();

        $albums = $manager->getRepository('AppBundle:Album')->findAll();
        $i = 0;
        foreach ($albums as $album) {
            $max = 0 == $i ? 25 : rand(5, 25);
            for ($i = 1; $i <= $max; $i++) {
                $image = new AlbumImage();
                $image->setAlbum($album);
                $image->setTitle(sprintf('Image %s from album %s', (string)$i, (string)$album->getId()));
                $image->setDescription(rand(0, 9) > 5 ? sprintf('Image %s %s', (string)$i, 'description') : null);
                $image->setPath('test.jpg');

                $manager->persist($image);
            }
            $i++;
        }
        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2;
    }


}