<?php
/*
 * Файл: SecurityController.php
 * 
 * Создан: 24.02.16
 * 
 * Часть программного продукта: martest_symfony
 *
 * TODO: Дополнительные условия
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Controller to handle security issues
 *
 * @author Alexander Pankov <ap@wdevs.ru>
 *
 * @todo Extend controller to handle user registration and all registration-related issues
 */
class SecurityController extends Controller
{
    /**
     * Login form
     *
     * @Route("/login", name="login")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $utils = $this->get('security.authentication_utils');

        $error = $utils->getLastAuthenticationError();

        $lastUsername = $utils->getLastUsername();

        return $this->render('@App/login/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }
}