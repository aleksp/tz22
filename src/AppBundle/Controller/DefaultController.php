<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Main application controller
 *
 * @author Alexander Pankov <ap@wdevs.ru>
 */
class DefaultController extends Controller
{
    /**
     * Get application index
     *
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('@App/index.html.twig');
    }

    /**
     * Get albums
     *
     * @Route("/albums", name="albums", condition="request.isXmlHttpRequest()")
     */
    public function albumsAction()
    {
        $user = $this->getUser();
        $albumService = $this->get('album_service');
        $albums = $albumService->getUserAlbums($user);

        return new Response($albums);
    }

    /**
     * Get single album
     *
     * @Route("/albums/view/{albumId}", name="albums_view", condition="request.isXmlHttpRequest()")
     *
     * @param int $albumId
     *
     * @return Response
     */
    public function albumShowAction($albumId)
    {
        $albumService = $this->get('album_service');
        $album = $albumService->getSingleAlbum($albumId);

        return new Response($album);
    }

    /**
     * Show images in selected album
     *
     * @Route("/albums/images/{albumId}/{page}", name="album_images")
     *
     * @param $albumId
     * @param int $page
     * @param int $perPage
     *
     * @return string|\Symfony\Component\Serializer\Encoder\scalar
     */
    public function imagesAction($albumId, $page = 1, $perPage = 10)
    {
        $service = $this->get('album_service');
        $album = $service->getAlbumById($albumId);

        return new Response($service->getAlbumImages($album, $page, $perPage));
    }
}
