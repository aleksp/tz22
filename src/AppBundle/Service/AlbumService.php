<?php
/*
 * Файл: AlbumService.php
 * 
 * Создан: 24.02.16
 * 
 * Часть программного продукта: martest_symfony
 *
 * TODO: Дополнительные условия
 */

namespace AppBundle\Service;

use AppBundle\Entity\Album;
use AppBundle\Entity\User;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;

/**
 * Service to work with albums and images
 *
 * @author Alexander Pankov <ap@wdevs.ru>
 */
class AlbumService
{
    /**
     * Doctrine entity manager
     *
     * @var EntityManager
     */
    private $em;

    /**
     * Data serializer
     *
     * @var Serializer
     */
    private $serializer;

    /**
     * KNP Paginator
     *
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * AlbumService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer($classMetadataFactory, new CamelCaseToSnakeCaseNameConverter())];

        $this->serializer = new Serializer($normalizers, $encoders);
    }


    /**
     * Get all albums that belongs to current user
     *
     * @param User $user
     * @return string|\Symfony\Component\Serializer\Encoder\scalar
     */
    public function getUserAlbums(User $user)
    {
        $albums = $this->em->getRepository('AppBundle:Album')->findBy([
            'author' => $user,
        ]);

        $data = $this->serializer->normalize($albums, null, [
            'groups' => [
                'show',
            ],
        ]);
        $returnValue = $this->serializer->serialize($data, 'json');

        return $returnValue;
    }

    /**
     * Get images for album
     *
     * @param Album $album
     * @param int $page
     * @param int $perPage
     * @return string|\Symfony\Component\Serializer\Encoder\scalar
     */
    public function getAlbumImages(Album $album, $page = 1, $perPage = 10)
    {
        $query = $this->em->createQueryBuilder()
            ->select('i')
            ->from('AppBundle:AlbumImage', 'i')
            ->leftJoin('i.album', 'a')
            ->where('a.id = :albumId')
            ->setParameter('albumId', $album->getId())
            ->getQuery();

        $pagination = $this->paginator->paginate($query, $page, $perPage);

        $data = $this->serializer->normalize($pagination->getItems(), null, [
            'groups' => [
                'show',
            ],
        ]);

        $returnValue = $this->serializer->serialize($data, 'json');

        return $returnValue;
    }

    /**
     * Get single album
     *
     * @param int $albumId
     * @param int $page
     * @param int $perPage
     * @return string|\Symfony\Component\Serializer\Encoder\scalar
     */
    public function getSingleAlbum($albumId, $page = 1, $perPage = 10)
    {
        $album = $this->getAlbumById($albumId);
        $data = $this->serializer->normalize($album, null, [
            'groups' => [
                'show',
            ],
        ]);

        return $this->serializer->serialize($data, 'json');
    }


    /**
     * Get album by album ID
     *
     * @param int $id Album ID
     *
     * @return Album|null|object
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getAlbumById($id)
    {
        $album = $this->em->find('AppBundle:Album', $id);
        if (!$album) {
            throw new NotFoundHttpException('Album not found');
        }

        return $album;
    }
}