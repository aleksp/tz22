<?php

namespace AppBundle\Doctrine\Traits;

/**
 * Trait to authomate "time stamps" functionality. Use HasLifecycleCallbacks for model.
 */
trait TimestampableTrait
{
    /**
     * Created at
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * Updated at
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    /**
     * Renews object data
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTimestamps()
    {
        $now = new \DateTime('now');
        if (null == $this->createdAt) {
            $this->createdAt = $now;
        }
        $this->updatedAt = $now;
    }

    /**
     * Getter for 'created_at'
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Setter for 'created_at'
     *
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Getter for 'updated_at'
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Setter for 'updated_at'
     *
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}