<?php

namespace AppBundle\Doctrine\Traits;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait authomates ID creation
 */
trait IntIdTrait
{
    /**
     * Integer ID
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"show"})
     */
    protected $id;

    /**
     * Getter for ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}